<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

     <!-- JUMBOTRON -->

<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('jumbotron_title'); ?></h1>
         <button><?php the_field('jumbotron_button'); ?></button>
       </div>
    </div>
</div>


   <div class="about_us_content">
       <div class="wrapper">
     <h2>About Us</h2>
     <p><?php the_field('about_us_text'); ?></p>
        <a href="/about-us">
            <button class='blue'>LEARN MORE</button>
        </a>
        </div>
   </div>


     <!-- PARTNER SLIDE -->
     <section class='partner_slider'>
      <div class="schedule_content">
       <h2>Teaming up with Basketball City</h2>
          <div class="slides">
              
                  <?php
            $args = array(
      'post_type' => 'partner_logos',
                'posts_per_page' => 99
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
              
              
              <div>
                  <a href="<?php the_field('link'); ?>" target="_blank">
                    <img src="<?php the_field('logo'); ?>">
                </a>
              </div>

                      
    <?php
            }
                  }
            else  {
            echo 'No Partners Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
            </div>
      </div>
     </section>





     <!-- ABOUT US SECTION -->
     <section class="about_us_section">
         <div class="about_us_map">
            <?php echo do_shortcode('[wpsl]'); ?>
             
         </div>
        
     </section>

     <!-- SCHEDULE SECTION -->
     <section class="rent_section" style="background-image:url(<?php the_field('rent_court_image'); ?>)">
       <div class="rent_content">
         <h2>Rent a Court</h2>
         <p><?php the_field('rent_court_text'); ?></p>
           <a href="/court-rentals">
            <button>RENT COURTS</button>
               </a>
       </div>
     </section>
     <!-- NEWS AND EVENTS SECTION -->
     <section class="news_events_section">
       <h2>News and Events</h2>
       <div class="news_events_content">
           
                             <?php
            $args = array(
      'post_type' => 'news',
                'posts_per_page' => 3
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
              
        <div class="news">
            <div class="image_holder">
                <img src="<?php the_field('image'); ?>">
                </div>
           <h4 class="news_title"><?php the_field('headline'); ?></h4>
           <p><?php the_field('short_intro'); ?>...</p>
         </div>
                      
    <?php
            }
                  }
            else  {
            echo 'No News Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>

       </div>
         <a href="/special-events">
            <button class="view_all blue">VIEW ALL</button>
         </a>
     </section>


<!--    SOCIAL MEDIA FEED SECTION-->

    <section>
        <?php echo do_shortcode('[instashow columns="6"]'); ?>
    </section>

<!--SMOOTH SCROLLING-->
<script>
$(".jumbotron_content button").click(function() {
    $('html, body').animate({
        scrollTop: $(".partner_slider").offset().top
    }, 2000);
});
</script>

<?php
get_footer();
