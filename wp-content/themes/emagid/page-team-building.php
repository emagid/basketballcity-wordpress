<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

   <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1 style="color:red !important;"><?php the_field('banner_title'); ?></h1>
       </div>
     </section>


     <!-- SCHEDULE SECTION -->
     <section class='list_section'>
      <div class="list_content">
       <h2>Basketball City Team Building</h2>
          <?php the_field('content'); ?>
         </div>
     </section>
      <!-- CONTACT SECTION -->
     <section class="contact" style="border-bottom:1px solid #fff;">
       <h3>Team Building Event, please email Dave or call (212) 233-5050</h3>
         <a href="/contact">
            <button>CONTACT NOW</button>
        </a>
     </section>

<?php
get_footer();
