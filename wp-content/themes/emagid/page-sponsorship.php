<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section>


     <!-- RENTING INFO SECTION -->

     <!-- SCHEDULE SECTION -->
     <section class='schedule_section'>
      <div class="schedule_content">
       <h2>INTERESTED IN SPONSORSHIP?</h2>
          <p><?php the_field('text'); ?></p>
         <a href="/contact">
       <button>MORE INFO</button>
              </a>
      </div>
     </section>



    <!-- FEATURES SECTION -->

     


      <!-- CONTACT SECTION -->
     <section class="court_contact">
       <h2>INTERESTED IN SPONSORSHIP?</h2>
        <?php echo do_shortcode('[contact-form-7 id="141" title="Contact"]'); ?>
     </section>

<script>
$(".jumbotron_content button").click(function() {
    $('html, body').animate({
        scrollTop: $(".court_contact").offset().top
    }, 2000);
});
</script>
<?php
get_footer();
