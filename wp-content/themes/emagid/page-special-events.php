<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('title'); ?></h1>
         <p><?php the_field('caption'); ?></p>
       </div>
    </div>
</div>


     <!-- SPECIAL EVENTS INFO SECTION -->
     <section class="special_event_info">
     	<div class="info_holder">
	     	<h2>SPECIAL EVENTS</h2>
	     	<p>BasketBall City hosts a number of different events every year. Look out for the next amazing event!</p>
     	</div>
     </section>


      <!-- EVENTS SECTION -->
     <section class="events">
     	<aside class="filters">
            <a class="selected" id="trigevents_2019">2019</a>
            <a id="trigevents_2018">2018</a>
     		<a id="trigevents_2017">2017</a>
     		<a >2016</a>
     		<a >2015</a>
     		<a >2014</a>
     		<a >2013</a>

     	</aside>
         
                       	<aside class="events_holder" id="events_2019">
            
                                         <?php
            $args = array(
      'post_type' => 'news',
                'cat' => '31',
          'posts_per_page' => -1     
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="event">
     			<img src="<?php the_field('image'); ?>">
     			<div class="event_text">
	     			<h3><?php the_field('headline'); ?></h3>
	     			<p class="short_intro"><?php the_field('short_intro'); ?>...</p>
                    <p class="full_text" style="display:none;"><?php the_field('full_text'); ?></p>
	     			<button class="read_more">READ MORE</button>
                    <button class="show_less" style="display:none;">Show Less</button>
     			</div>
     		</div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>

     	</aside>
              	<aside class="events_holder" id="events_2018" style="display:none;">
            
                                         <?php
            $args = array(
      'post_type' => 'news',
                'cat' => '17'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="event">
     			<img src="<?php the_field('image'); ?>">
     			<div class="event_text">
	     			<h3><?php the_field('headline'); ?></h3>
	     			<p class="short_intro"><?php the_field('short_intro'); ?>...</p>
                    <p class="full_text" style="display:none;"><?php the_field('full_text'); ?></p>
	     			<button class="read_more">READ MORE</button>
                    <button class="show_less" style="display:none;">Show Less</button>
     			</div>
     		</div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>

     	</aside>
     	<aside class="events_holder" id="events_2017" style="display:none;">
            
                                         <?php
            $args = array(
      'post_type' => 'news',
                'cat' => '7'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="event">
     			<img src="<?php the_field('image'); ?>">
     			<div class="event_text">
	     			<h3><?php the_field('headline'); ?></h3>
	     			<p class="short_intro"><?php the_field('short_intro'); ?>...</p>
                    <p class="full_text" style="display:none;"><?php the_field('full_text'); ?></p>
	     			<button class="read_more">READ MORE</button>
                    <button class="show_less" style="display:none;">Show Less</button>
     			</div>
     		</div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>

     	</aside>
     </section>

<script>
    $(".event").click(function(){
    $(".read_more",this).toggle();
    $(".show_less",this).toggle();
    $("p.full_text",this).toggle(500);
    $("p.short_intro",this).toggle();
        
});
    
//$(".show_less").click(function(){
//    $(this).hide();
//    $(".read_more").show();
//    $("p.full_text").hide();
//    $("p.short_intro").show(1000);
//        
//});
    $(".filters a").click(function(){
        $( ".filters a" ).removeClass( "selected" );
        $( this ).addClass( "selected" );
       
});
    $(".filters a#trigevents_2019").click(function(){
    $( ".events_holder" ).hide();
    $( ".events_holder#events_2019" ).show();
       
});
$(".filters a#trigevents_2018").click(function(){
    $( ".events_holder" ).hide();
    $( ".events_holder#events_2018" ).show();
       
});
    $(".filters a#trigevents_2017").click(function(){
    $( ".events_holder" ).hide();
    $( ".events_holder#events_2017" ).show();
       
});
</script>
<?php
get_footer();
