<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- MAPS -->
     <section class="maps">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.3693591414726!2d-73.98716728459512!3d40.709884779332185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a2c172236dd%3A0x6b82a58f09540466!2sBasketball+City!5e0!3m2!1sen!2sus!4v1503675419830" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
     </section>

      <!-- CONTACT SECTION -->
     <section class="contact_page">
       <h1>Get in touch with us at Basketball City</h1>
     </section>

    <section class="contact_form">
        <div class="contact_form_wrapper">
        <?php echo do_shortcode('[contact-form-7 id="141" title="Contact"]'); ?>
            </div>
        <div class="contact_form_info">
                    <div class="schedule">
          <div class="title">
            <h4>Contact Information</h4>
            <p class="age"><span class="ages">(212)-233-5050 ext. 210</span></p>
          </div>
          <div class="youtube social">
            <img src="">
            <p><span class="light">BasketBallCity23</span></p>
          </div>
          <div class="twitter social">
            <img src="">
            <p><span class="light">@BasketBallCity</span></p>
          </div>
          <div class="facebook social">
            <img src="">
            <p><span class="light">Basketball City</span></p>
          </div>
            <div class="snapchat social">
            <img src="">
            <p><span class="light">@Basketball_City</span></p>
          </div>
        </div>
        </div>
</section>
<?php
get_footer();
