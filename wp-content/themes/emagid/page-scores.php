<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_title();?></h1>
       </div>
     </section>


     <!-- SPECIAL EVENTS INFO SECTION -->
     <section class="special_event_info">
     	<div class="info_holder">
	     	<?php the_field('title'); ?>
     	</div>
     </section>


      <!-- EVENTS SECTION -->
     <section class="events">
     	<aside class="filters">
            <a id="all" class="selected">Monday Central</a>
            <a id="dunks">Monday North</a>
            <a id="winners">Tuesday-Wednesday North</a>
     		<a id="adult">Tuesday-Wednesday Atlantic</a>
     		<a id="youth">Tuesday-Wednesday Pacific</a>
            <a id="youth">Tuesday-Wednesday South</a>
            <a id="youth">Tuesday-Wednesday West</a>
            <a id="youth">Wednesday East</a>
            <a id="youth">Thursday North</a>
            <a id="youth">Thursday Central</a>
            <a id="youth">Thursday South</a>
     	</aside>
              	<aside class="events_holder" id="events_2018" style="margin:0 auto;">
            
                                         <?php
            $args = array(
      'post_type' => 'scores_highlights'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="event">
     			<img src="<?php the_field('image'); ?>">
     			<div class="event_text">
                    <p><?php the_field('date'); ?></p>
	     			<h3>
                        <?php the_field('team_1'); ?>(a) <span><?php the_field('team_1_score'); ?></span> @
                        <?php the_field('team_2'); ?>(h) <span><?php the_field('team_2_score'); ?></span>
                    </h3>
	     			<p class="short_intro"><?php the_field('quick_recap'); ?>...</p>
                    <a href="<?php the_permalink(); ?>">
	     			   <button>READ MORE</button>
                    </a>
     			</div>
     		</div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>

     	</aside>
     	<aside class="events_holder" id="events_2017" style="display:none;">
            
                                         <?php
            $args = array(
      'post_type' => 'news',
                'cat' => '7'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="event">
     			<img src="<?php the_field('image'); ?>">
     			<div class="event_text">
	     			<h3><?php the_field('headline'); ?></h3>
	     			<p class="short_intro"><?php the_field('short_intro'); ?>...</p>
                    <p class="full_text" style="display:none;"><?php the_field('full_text'); ?></p>
	     			<button class="read_more">READ MORE</button>
                    <button class="show_less" style="display:none;">Show Less</button>
     			</div>
     		</div>
            
            
                <?php
            }
                  }
            else  {
            echo 'No Events Found';
            }
      ?>

     	</aside>
     </section>

<script>

    
//$(".show_less").click(function(){
//    $(this).hide();
//    $(".read_more").show();
//    $("p.full_text").hide();
//    $("p.short_intro").show(1000);
//        
//});

</script>
<?php
get_footer();
