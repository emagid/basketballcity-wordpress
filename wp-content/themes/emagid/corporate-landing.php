<?php
/*
 * Template Name: Landing Template - Corporate
 */

get_header('landing'); ?>




      <!-- CONTACT SECTION -->

<div class="landing_container">
     <section class="contact youth_info landing_section">
         <h2><?php the_field('banner_title'); ?></h2>
      <div class="youth_info_holder">
          <p><?php the_field('youth_programs'); ?></p>

      </div>
     </section>
         <section class="landing_contact">
         <h2>Contact Us</h2>
      <div class="youth_info_holder">
          	
<?php echo do_shortcode('[contact-form-7 id="1319" title="Corporate League Request"]'); ?>
          
      </div>
     </section>

                   <section class="pictures">
         
    <?php
            $args = array(
      'post_type' => 'gallery',
        'posts_per_page' => 4,
                'cat' => '33'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
         
       <div class="pic" style="background-image: url(<?php the_field('image'); ?>);">
         <div class="overlay">
           <p><?php the_field('title'); ?></p>
         </div>
       </div>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
         
         
    
     </section>
</div>

<div class="landing_contact_info"> 
            <div class="contact_form_info contact_form_info_landing">
                    <div class="schedule">
          <div class="title">
            <h4>Contact Information</h4>
            <p class="age"><span class="ages">(212)-233-5050 ext. 210</span></p>
          </div>
          <div class="youtube social">
            <img src="">
            <p><span class="light">BasketBallCity23</span></p>
          </div>
          <div class="twitter social">
            <img src="">
            <p><span class="light">@BasketBallCity</span></p>
          </div>
          <div class="facebook social">
            <img src="">
            <p><span class="light">Basketball City</span></p>
          </div>
            <div class="snapchat social">
            <img src="">
            <p><span class="light">@Basketball_City</span></p>
          </div>
        </div>
        </div>
</div>

    <style>
        div.wpcf7-response-output {
            color:#fff;
        }
</style>
    
