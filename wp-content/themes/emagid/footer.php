<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>


     <!-- FOOTER SECTION -->
     <footer>
       <div class="footer_section">
         <h4 class="footer_title">See More</h4>
           	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "footer-1"
    )); ?>
       </div>
       <div class="footer_section">
         <h4 class="footer_title">Events / Programs</h4>
                      	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "footer-2"
    )); ?>
       </div>
       <div class="footer_section">
         <h4 class="footer_title">Contact Us</h4>
           <?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
    <ul id="sidebar">
        <?php dynamic_sidebar('sidebar-2'); ?>
    </ul>
<?php } ?>

         <div class="icon_holder footer">
<!--            Facebook-->
            <a href="https://www.facebook.com/BasketBallCityNYC" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_facebook.png">
            </a>
<!--            Twitter-->
            <a href="https://twitter.com/basketballcity" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_twitter.png">
            </a>
<!--            YouTube-->
            <a href="https://www.youtube.com/user/basketballcity23" target="_blank">    
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_video.png">
            </a>
<!--            Snapchat-->
            <a href="https://www.snapchat.com/add/Basketball_City" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_snapchat.png">
            </a>
            
            <a href="mailto:info@basketballcity.com">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_letter.png">
            </a>
        </div>
       </div>
       <div class="right footer_section">
        <a href="https://basketballcity.leagueapps.com/login"><button>Account Log In</button></a>
         <button id="btn_join">JOIN OUR EMAIL LIST!</button>
           <?php echo do_shortcode('[wysija_form id="2"]'); ?>
           
         <p>For Email Newsletters you can trust!</p>
           <p><a href="https://basketballcity.leagueapps.com/sms">Want to receive text message updates instead?</a></p>
       </div>
         
         
     </footer>

     <section class="copyright">
       <p>Copyright 2019 BasketballCity.com   <span class="divider">|</span>   Developed by eMagid.com</p>
     </section>

<script>
    $("#btn_join").click(function(){
    $("footer .widget_wysija_cont.shortcode_wysija").toggle();
});

</script>

<?php wp_footer(); ?>

</body>
</html>
