<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
       </div>
     </section>


     <!-- SPECIAL EVENTS INFO SECTION -->
     <section class="special_event_info">
     	<div class="info_holder">
	     	<h2><?php the_title();?></h2>
     	</div>
     </section>


      <!-- EVENTS SECTION -->
     <section class="events">
<!--
     	<aside class="filters">
            <a class="selected" id="trigevents_2018">Week 1</a>
     		<a id="trigevents_2017">Week 2</a>
     		<a >Week 3</a>

     	</aside>
-->
              	<aside class="events_holder" id="events_2018" style="margin:0 auto;">
<?php the_field('game_summary'); ?>



     	</aside>

     </section>

<script>

    
//$(".show_less").click(function(){
//    $(this).hide();
//    $(".read_more").show();
//    $("p.full_text").hide();
//    $("p.short_intro").show(1000);
//        
//});

</script>
<?php
get_footer();
