<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->
<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1>HIGHLIGHT CORNER</h1>
       </div>
    </div>
</div>

      <!-- EVENTS SECTION -->
     <section class="events">
     	<aside class="filters">
            <a id="all" class="selected">All</a>
            <a id="dunks">Dunks</a>
            <a id="winners">Game Winners</a>
     		<a id="adult">Adult League</a>
     		<a id="youth">Youth League</a>
     	</aside>
     	<aside class="events_holder" >
            
            
            <div class="video_content" id="show_all">
    <?php
            $args = array(
      'post_type' => 'highlight_corner'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="highlight_videos">
                <div class="video_container">
                    <a class="lightBoxVideoLink"  rel="prettyPhoto" href="<?php the_field('video_link'); ?>">
                        <video width="100%" height="240" controls>
                          <source src="<?php the_field('video_link'); ?>">
                        Your browser does not support the video tag.
                        </video>

                    </a>
     			
                    </div>
     			<div class="event_text">
	     			<h3><?php the_field('title'); ?></h3>
	     			<p><?php the_field('short_intro'); ?></p>
     			</div>
     		</div>
            
                            <?php
            }
                  }
            else  {
            echo 'No Videos Found';
            }
      ?>
            </div>

        <div class="video_content" id="show_dunks" style="display:none;">
        

            
    <?php
            $args = array(
      'post_type' => 'highlight_corner',
                'cat' => '5'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="highlight_videos">
                <div class="video_container">
                    <a class="lightBoxVideoLink"  href="<?php the_field('video_link'); ?>">
                        <video width="100%" height="240" controls>
                          <source src="<?php the_field('video_link'); ?>">
                        Your browser does not support the video tag.
                        </video>

                    </a>
     			
                    </div>
     			<div class="event_text">
	     			<h3><?php the_field('title'); ?></h3>
	     			<p><?php the_field('short_intro'); ?></p>
     			</div>
     		</div>
            
                            <?php
            }
                  }
            else  {
            echo 'No Videos Found';
            }
      ?>
                    </div>
 
        
        
         
             <div class="video_content" id="show_winners" style="display:none;">
            
    <?php
            $args = array(
      'post_type' => 'highlight_corner',
                'cat' => '6'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="highlight_videos">
                <div class="video_container">
                    <a class="lightBoxVideoLink"  href="<?php the_field('video_link'); ?>">
                        <video width="100%" height="240" controls>
                          <source src="<?php the_field('video_link'); ?>">
                        Your browser does not support the video tag.
                        </video>

                    </a>
     			
                    </div>
     			<div class="event_text">
	     			<h3><?php the_field('title'); ?></h3>
	     			<p><?php the_field('short_intro'); ?></p>
     			</div>
     		</div>
            
                            <?php
            }
                  }
            else  {
            echo 'No Videos Found';
            }
      ?>
        </div>
    
         
         
         
             <div class="video_content" id="show_adult" style="display:none;">
            
    <?php
            $args = array(
      'post_type' => 'highlight_corner',
                'cat' => '3'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="highlight_videos">
                <div class="video_container">
                    <a class="lightBoxVideoLink"  href="<?php the_field('video_link'); ?>">
                        <video width="100%" height="240" controls>
                          <source src="<?php the_field('video_link'); ?>">
                        Your browser does not support the video tag.
                        </video>

                    </a>
     			
                    </div>
     			<div class="event_text">
	     			<h3><?php the_field('title'); ?></h3>
	     			<p><?php the_field('short_intro'); ?></p>
     			</div>
     		</div>
            
                            <?php
            }
                  }
            else  {
            echo 'No Videos Found';
            }
      ?>
        </div>
         
                      <div class="video_content" id="show_youth" style="display:none;">
            
    <?php
            $args = array(
      'post_type' => 'highlight_corner',
                'cat' => '4'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
     		<div class="highlight_videos">
                <div class="video_container">
                    <a class="lightBoxVideoLink"  href="<?php the_field('video_link'); ?>">
                        <video width="100%" height="240" controls>
                          <source src="<?php the_field('video_link'); ?>">
                        Your browser does not support the video tag.
                        </video>

                    </a>
     			
                    </div>
     			<div class="event_text">
	     			<h3><?php the_field('title'); ?></h3>
	     			<p><?php the_field('short_intro'); ?></p>
     			</div>
     		</div>
            
                            <?php
            }
                  }
            else  {
            echo 'No Videos Found';
            }
      ?>
        </div>
         </aside>
     </section>

<script>
$('.lightBoxVideoLink').simpleLightbox();
</script>

<script>

$( document ).ready(function() {
    $('.video_content').hide();
    $('#show_all').show();
});
    
$('.filters a').on('click', function () {
    $('.filters a').removeClass('selected');
    $(this).addClass('selected');
});
    
$('.filters a#all').on('click', function () {
    $('.video_content').hide();
    $('#show_all').show();
});
    $('.filters a#dunks').on('click', function () {
    $('.video_content').hide();
    $('#show_dunks').show();
});
    $('.filters a#winners').on('click', function () {
    $('.video_content').hide();
    $('#show_winners').show();
});
    $('.filters a#adult').on('click', function () {
    $('.video_content').hide();
    $('#show_adult').show();
});
    $('.filters a#youth').on('click', function () {
    $('.video_content').hide();
    $('#show_youth').show();
});

</script>
<?php
get_footer();
