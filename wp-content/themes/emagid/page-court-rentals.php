<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->


<!--
<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1>RENT A COURT</h1>
         <button>RENT NOW</button>
       </div>
    </div>
</div>
-->
     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section>


     <!-- RENTING INFO SECTION -->
    <section class="white_background_info">

     </section>

     <!-- SCHEDULE SECTION -->
     <section class='schedule_section'>
      <div class="schedule_content">
       <h2>COURT RENTALS</h2>
       <p>Charter court rentals are available at Basketball City! Whether you're interested in renting a court for a corporate outing or simply want to get together with a bunch of friends, Basketball City can accommodate you. We offer 7 hardwood full courts, locker rooms, seating, basketballs, and showers. If you want to get serious we can arrange for referees and scorekeepers.</p>
          
          <a href="/contact">
       <button>MORE INFO</button>
              </a>
      </div>
     </section>

     


      <!-- CONTACT SECTION -->
     <section class="court_contact blue_background_info">
       <h2>SCHEDULE A RENTAL</h2>
         <?php echo do_shortcode('[contact-form-7 id="161" title="Court Rental"]'); ?>
     </section>

<script>
$(".jumbotron_content button").click(function() {
    $('html, body').animate({
        scrollTop: $(".court_contact").offset().top
    }, 2000);
});
</script>
<?php
get_footer();
