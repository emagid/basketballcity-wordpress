<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('jumbotron_title'); ?></h1>
         <button id="register">SIGN UP NOW</button>
       </div>
    </div>
</div>


     <!-- LEAGUE INFO SECTION -->
     <section class="league_info" id="adults">
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/book.png">
        <button id="rules">LEAGUE RULES</button>
       </div>
<!--
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leagues.jpg">

                <button id="divisions">LEAGUE SCHEDULE</button>

       </div>
-->
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/previous_seasons.jpg">

                <button id="standings">LEAGUE SCHEDULE / STANDINGS</button>

       </div>
         
     </section>

<section class="league_content">
            <div class="rules_content" style="display:none;">
                <?php the_field('rules'); ?>
             </div>
    
    
    
<script type='text/javascript' src='https://basketballcity.leagueapps.com/widgets/registrationListings?bUseSkin=false&skillLevel=78438&season=0&format=1&programType=LEAGUE&heightSetting=800px'></script>
<div id="registration-listings" style="background: #e2e2e2; border-top: 1px solid #c2c2c2;-moz-border-radius: 0 0 4px 4px; -webkit-border-radius: 0 0 4px 4px;border-radius: 0 0 4px 4px;"><div style="border-top: 1px solid #D2D2D2;padding: 0 30px 2px 30px;"></div></div>
    
    <div class="schedule_content" style="display:none;">
        <ul class="divisions">

                <?php
            $args = array(
      'post_type' => 'adult_league',
      'posts_per_page' => 99
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
            <li>
                <a href="<?php the_field('link'); ?>">
                    <button class="blue"><?php the_field('division'); ?></button>
                </a>
            </li>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>

<!--             <li>
                <a href="http://basketballcity.leagueapps.com/leagues/443775/schedule">
                    <button class="blue">Monday Central</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/443778/schedule">
                    <button class="blue">Monday North</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/443889/schedule">
                    <button class="blue">Monday West</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/447963/schedule">
                    <button class="blue">Tuesday Central</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/447036/schedule">
                    <button class="blue">Tuesday North</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/443940/schedule">
                    <button class="blue">Tuesday West</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/444165/schedule">
                    <button class="blue">Wednesday East</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/442881/schedule">
                    <button class="blue">Thursday Central</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/439788/schedule">
                    <button class="blue">Thursday North</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/442884/schedule">
                    <button class="blue">Thursday South</button>
                </a>
            </li>
                        <li>
                <a href="http://basketballcity.leagueapps.com/leagues/232599/schedule">
                    <button class="blue">TV/AD Media League</button>
                </a>
            </li> -->
            
        </ul>
    </div>
    
</section>


      <!-- CONTACT SECTION -->
     <section class="contact">
       <h3>Email <a href="mailto:dave@basketballcity.com" target="_blank">Dave Schoenholt</a> or call (212) 233-5050 to register your team in one of our leagues.</h3>
         <a href="mailto:dave@basketballcity.com" target="_blank">
            <button>CONTACT NOW</button>
        </a>
     </section>

     <!-- SCHEDULE SECTION -->
<!--
     <section class='schedule_section'>
      <div class="schedule_content">
       <img src="<?php echo get_template_directory_uri(); ?>/assets/img/schedule.png">
       <h2>Schedule & Standings</h2>
                  <a href="http://basketballcity.leagueapps.com/" target="_blank">
                <button>Monday Central</button>
            </a>
      </div>
     </section>
-->


      <!-- PICTURE SECTION -->
     <section class="pictures">
         
    <?php
            $args = array(
      'post_type' => 'gallery',
        'posts_per_page' => 99,
                'cat' => '3'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
         
       <div class="pic" style="background-image: url(<?php the_field('image'); ?>);">
         <div class="overlay">
           <p><?php the_field('title'); ?></p>
         </div>
       </div>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
         
         
    
     </section>

<script>
$("#register").click(function(){
        $('html, body').animate({
        scrollTop: $(".league_content").offset().top
    }, 2000);
    $(".league_content > div").hide();
        $(".register_content").toggle();
});
    
    $("#rules").click(function(){
        $('.info_holder button').removeClass('show_less');
        $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".rules_content").toggle();
});
    
        $("#divisions, #standings").click(function(){
            $('.info_holder button').removeClass('show_less');
            $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".schedule_content").toggle();
});
</script>
<?php
get_footer();
