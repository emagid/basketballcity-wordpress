<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

     <!-- JUMBOTRON -->
<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
    </div>
</div>

     <!-- JUMBOTRON -->

<!--      <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section> -->


     <!-- RENTING INFO SECTION -->
    <section class="community_info blue_background_info">
       <p><?php the_field('blue_text_block'); ?></p>
     </section>


     <!-- QUOTES SECTION -->
     <section class='list_section'>
      <div class="list_content" id="quotes">
       <h2>A few words from those who've been impacted by Basketball City's outreach initiatives</h2>
       <ul>
          <li><p>"<span>I have attended Basketball City since 2013, I am proud to say that Basketball City has been very helpful and has gave me many opportunities to attend many activities. I am very grateful that i found this program because it has changed my life in a positive way and I will be thankful for accepting me into the family.</span>"</p></li>
          <li><p>"<span>I would like to thank you for allowing my son Ethan to attend Basketball City through the Vladecks house. My son loves it there. When he gets home from a day at Basketball City he can not stop talking about his experience there. It has enforced his love for basketball by ten folds. He told me he wants to play for the New York Knicks when he gets older. My son won his 1st trophy at Basketball City and he loves it. He made sure I put his trophy in the living room so everyone can see it. The staff there are amazing with the kids. I feel I can leave my son there and he would be in the best care. I would recommend Basketball City to any parent. Again a big thank you from my son and I. I look forward to sending my son there in the near future.</span>"</p></li>
         <li><p>"<span>My son, David, enjoyed attending the BasketBall City program. His excitment to see the coaches and play with his friends is always good to see. Basketball have given David so much confidence to pursue his dream of one day playing professions ball. Without the scholarship you provided, David would never be able to attend a program like yours. Again, Thank you to Mr. Kenny and the rest of the BaskBall City Staff for the opportunity.</span>"</p></li>
        </ul>
         </div>
     </section>
<?php
get_footer();
