<?php

get_header(); ?>

   <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_title();?></h1>
       </div>
     </section>

      <!-- CONTACT SECTION -->
     <section class="contact youth_info">
         <div class="program_text">
         <?php the_field('text'); ?>
        </div>
     </section>


      <!-- SCHEDULE SECTION -->
<!--
      <section class="schedules inner">
          <//?php the_field('leagueapps_script'); ?>
      </section>
-->
<section class="schedules inner">
    <div class="register_logos">
        <div>
            <a href="https://basketballcity.leagueapps.com/camps/891337-basketball-city-summer-camps-2019" target="_blank"> 
                <img src="/wp-content/uploads/2018/03/1rvdBLz8.jpg">
                <p><strong>Register for<br> Summer Camp 2019</strong></p>
            </a>
        </div>
<!--
        <div>
            <a href="https://basketballcity.leagueapps.com/camps/619476-basketball-city-summer-camps-2018-nbpa" target="_blank">             <img src="/wp-content/uploads/2018/03/nbpa-logo-header.png">
                <p><strong>Register for<br> NBPA Summer Camp 2018</strong></p>
            </a> 
        </div>
-->
    </div>
</section>
      <!-- SCHEDULE SECTION -->
      <section class="community_info blue_background_info">
          <?php the_field('forms_section'); ?>
      </section>


<?php
get_footer();
    
    
