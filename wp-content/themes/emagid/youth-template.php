<?php
/*
 * Template Name: Youth Template
 */

get_header(); ?>

   <!-- JUMBOTRON -->

<?php if (get_field('jumbotron_video') != ''): ?>
<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <?php the_title( '<h1>', '</h1>' ); ?>
       </div>
    </div>
</div>
<?php endif; ?>

<?php if (get_field('banner_image') != ''): ?>
     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section>
<?php endif; ?>

      <!-- CONTACT SECTION -->
     <section class="contact youth_info">
         <div class="program_text">
         <?php the_field('text'); ?>
        </div>
     </section>

      <!-- FORMS SECTION -->
      <section class="community_info blue_background_info">
          <?php the_field('forms_section'); ?>
      </section>


      <!-- SCHEDULE SECTION -->
      <section class="schedules inner">
          <?php the_field('leagueapps_script'); ?>
      </section>




<?php
get_footer();
    
    
