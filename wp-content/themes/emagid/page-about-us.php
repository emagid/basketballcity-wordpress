<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
    </div>
</div>
<!--
     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section>
-->


     <!-- QUOTES SECTION -->
     <section class='list_section'>
      <div class="list_content" id="about_us">
          <p>
<?php the_field('intro'); ?></p>
         </div>
     </section>


      <!-- PICTURE SECTION -->
     <section class="pictures">
         
                       <?php
            $args = array(
      'post_type' => 'gallery',
        'posts_per_page' => 99,
                'cat' => '3'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
         
       <div class="pic" style="background-image: url(<?php the_field('image'); ?>);">
         <div class="overlay">
           <p><?php the_field('title'); ?></p>
         </div>
       </div>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
         
         
    
     </section>

     <section class='list_section'>
      <div class="list_content" id="about_us">
          <?php the_field('directions'); ?>
         </div>
     </section>


     <!-- RENTING INFO SECTION -->
    <section class="community_info blue_background_info">
        <h2>Contact Information</h2>
        
                               <?php
            $args = array(
      'post_type' => 'team'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?> 

        
                        <div class="contact_box">
            <p><strong><?php the_field('name'); ?></strong></p>
            <a href = "mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
                            <p><?php the_field('title'); ?></p>
        </div>
              <?php
            }
                  }
            else  {
            echo 'No Team Found';
            }
      ?>
        
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/symbol_phone.png">
            <p><strong>Phone</strong></p>
            <a>(212) 233-5050</a>
        </div>
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/symbol_locate.png">
            <p><strong>Address</strong></p>
            <a>Pier 36 @ 299 South St. New York, NY 10002</a>
        </div>
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/symbol_fax.png">
            <p><strong>Fax</strong></p>
            <a>(212) 233-6161</a>
        </div>
        
        <div class="about_image">
            <img src="<?php the_field('about_us_image'); ?>">
        </div>
     </section>



<?php
get_footer();
