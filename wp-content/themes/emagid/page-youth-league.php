<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>

     <!-- JUMBOTRON -->

<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
         <button id="signup">SIGN UP NOW</button>
       </div>
    </div>
</div>


     <!-- YOUTH INFO SECTION -->
     <section class="league_info youth_info">
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/previous_seasons.jpg">

         <button id="faq">YOUTH FAQ</button>
           
       </div>
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/leagues.jpg">
  
         <button id="register">Registration</button>

       </div>
<!--
       <div class="info_holder">
         <img src="<?php echo get_template_directory_uri(); ?>/assets/img/previous_seasons.jpg">
           <a href="https://basketballcity.leagueapps.com" target="_blank">
         <button>MEET THE STAFF</button>
           </a>
       </div>
-->
     </section>

<section class="league_content">
            <div class="faq_content" style="display:none;">
                    <?php the_field('faq'); ?>
             </div>
    
    
    <div class="schedule_content" style="display:none;">
        <script type='text/javascript' src='https://basketballcity.leagueapps.com/widgets/leagueListings'></script>
    </div>
    
        <div class="register_content" style="display:none;">
<script type='text/javascript' src='https://basketballcity.leagueapps.com/widgets/registrationListings?bUseSkin=false&programType=CAMP&sport=0&season=0&heightSetting=400px'></script>
<div id="registration-listings" style="background: #e2e2e2; border-top: 1px solid #c2c2c2;-moz-border-radius: 0 0 4px 4px; -webkit-border-radius: 0 0 4px 4px;border-radius: 0 0 4px 4px;"><div style="border-top: 1px solid #D2D2D2;padding: 0 30px 2px 30px;"></div></div>
</section>



      <!-- CONTACT SECTION -->
     <section class="contact youth_info">
       <h2>YOUTH PROGRAMS</h2>
      <div class="youth_info_holder">
          <p><?php the_field('youth_programs'); ?></p>
      </div>
         <a href="https://basketballcity.leagueapps.com" target="_blank">
         </a>
     </section>


      <!-- SCHEDULE SECTION -->
      <section class="schedules">
<script type='text/javascript' src='https://basketballcity.leagueapps.com/widgets/registrationListings?bUseSkin=false&programType=EVENT&format=0&heightSetting=800px'></script>
<div id="registration-listings" style="background: #e2e2e2; border-top: 1px solid #c2c2c2;-moz-border-radius: 0 0 4px 4px; -webkit-border-radius: 0 0 4px 4px;border-radius: 0 0 4px 4px;"><div style="border-top: 1px solid #D2D2D2;padding: 0 30px 2px 30px;"></div></div>
          
          <script type='text/javascript' src='https://basketballcity.leagueapps.com/widgets/registrationListings?bUseSkin=false&programType=CAMP&format=0&heightSetting=1600px'></script>
<div id="registration-listings" style="background: #e2e2e2; border-top: 1px solid #c2c2c2;-moz-border-radius: 0 0 4px 4px; -webkit-border-radius: 0 0 4px 4px;border-radius: 0 0 4px 4px;color:black;"><div style="border-top: 1px solid #D2D2D2;padding: 0 30px 2px 30px;color:black;"></div></div>
<!--
        <div class="schedule">
          <div class="title">
            <h4>SUMMER CAMP 2017</h4>
            <p class="age"><span class="ages">AGES</span> 7 – 9</p>
          </div>
          <div class="date">
            <img src="">
            <p>June 19 - August 31 <span class="light">(11 weeks)</span></p>
          </div>
          <div class="time">
            <img src="">
            <p>Monday - Thursday <span class="light">9:00am - 4:00pm</span></p>
            <p>Friday <span class="light">9:00am - 3:00pm</span></p>
          </div>
          <div class="location">
            <img src="">
            <p>Hunter College (week 1 & 2)/Basketball City</p>
          </div>
        </div>

        <div class="schedule">
          <div class="title">
            <h4>SUMMER CAMP 2017</h4>
            <p class="age"><span class="ages">AGES</span> 10-12</p>
          </div>
          <div class="date">
            <img src="">
            <p>June 19 - August 31 <span class="light">(11 weeks)</span></p>
          </div>
          <div class="time">
            <img src="">
            <p>Monday - Thursday <span class="light">9:00am - 4:00pm</span></p>
            <p>Friday <span class="light">9:00am - 3:00pm</span></p>
          </div>
          <div class="location">
            <img src="">
            <p>Hunter College (week 1 & 2)/Basketball City</p>
          </div>
        </div>

        <div class="schedule">
          <div class="title">
            <h4>SUMMER CAMP 2017</h4>
            <p class="age"><span class="ages">AGES</span> 13-15</p>
          </div>
          <div class="date">
            <img src="">
            <p>June 19 - August 31 <span class="light">(11 weeks)</span></p>
          </div>
          <div class="time">
            <img src="">
            <p>Monday - Thursday <span class="light">9:00am - 4:00pm</span></p>
            <p>Friday <span class="light">9:00am - 3:00pm</span></p>
          </div>
          <div class="location">
            <img src="">
            <p>Hunter College (week 1 & 2)/Basketball City</p>
          </div>
        </div>

        <div class="schedule">
          <div class="title">
            <h4>SUMMER CAMP 2017</h4>
            <p class="age"><span class="ages">AGES</span> 16-17</p>
          </div>
          <div class="date">
            <img src="">
            <p>June 19 - August 31 <span class="light">(11 weeks)</span></p>
          </div>
          <div class="time">
            <img src="">
            <p>Monday - Thursday <span class="light">9:00am - 4:00pm</span></p>
            <p>Friday <span class="light">9:00am - 3:00pm</span></p>
          </div>
          <div class="location">
            <img src="">
            <p>Hunter College (week 1 & 2)/Basketball City</p>
          </div>
        </div>
-->
      </section>


<script>
$("#signup").click(function(){
        $('html, body').animate({
        scrollTop: $(".league_content").offset().top
    }, 2000);
    $(".league_content > div").hide();
        $(".register_content").toggle();
});
    
    $("#faq").click(function(){
        $('.info_holder button').removeClass('show_less');
        $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".faq_content").toggle();
});
    
        $("#register").click(function(){
            $('.info_holder button').removeClass('show_less');
            $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".register_content").toggle();
});
</script>

<?php
get_footer();
    
    
