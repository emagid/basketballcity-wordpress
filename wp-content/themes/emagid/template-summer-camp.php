<?php
/*
 * Template Name: Camp Template
 */
get_header(); ?>


     <!-- JUMBOTRON -->
<?php if (get_field('jumbotron_video') != ''): ?>
<div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay muted loop class="fillWidth">
            <source src="<?php the_field('jumbotron_video'); ?>" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="PATH_TO_WEBM" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <div class="jumbotron_content">
         <?php the_title( '<h1>', '</h1>' ); ?>
       </div>
    </div>
</div>
<?php endif; ?>

<?php if (get_field('banner_image') != ''): ?>
     <section class="jumbotron" style="background-image: url(<?php the_field('banner_image'); ?>);">
       <div class="jumbotron_content">
         <h1><?php the_field('banner_title'); ?></h1>
       </div>
     </section>
<?php endif; ?>

     <!-- QUOTES SECTION -->
     <section class='list_section'>
      <div class="list_content" id="about_us">
          <p>
<?php the_field('intro'); ?></p>
         </div>
     </section>

       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
      <!-- PICTURE SECTION -->
     <section class="pictures">
         
                       <?php
            $args = array(
      'post_type' => 'gallery',
                'cat' => '15',
             'posts_per_page' => 8,   
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
         
       <div class="pic" style="background-image: url(<?php the_field('image'); ?>);">
         <div class="overlay">
           <p><?php the_field('title'); ?></p>
         </div>
       </div>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
         
         
    
     </section>

     <!-- RENTING INFO SECTION -->
<!--
    <section class="community_info blue_background_info">
        <h2>Essential Forms</h2>
        
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.png">
            <a href="/wp-content/uploads/2018/03/Waiver-and-Release-2018.pdf" target="blank">
                <p><strong>Waiver and Release 2018</strong></p>
            </a>
        </div>
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.png">
            <a href="/wp-content/uploads/2018/03/Medical-Form.pdf" target="blank">
                <p><strong>Medical Form</strong></p>
            </a>
        </div>
        <div class="contact_box">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.png">
            <a href="/wp-content/uploads/2018/03/Youth-Programs-FAQ.pdf" target="blank">
                <p><strong>Youth Programs FAQs</strong></p>
            </a>
        </div>

    
     </section>
-->

<?php
get_footer();
