<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <!--JQUERY-->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
    
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"/>

    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5lightbox.js"></script>
    
    
    


	<?php wp_head(); ?>
    
<!--
    <script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-65170901-1', 'auto');

  ga('send', 'pageview');



</script>
-->
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108537015-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108537015-1');
</script>

</head>

<body <?php body_class(); ?>>

    <header>
      <nav class="left_nav">
	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "left"
    )); ?>
      </nav>

      <div class="logo_middle">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo.png">
            </a>
      </div>

      <nav class="right_nav">
          	 		<?php 
		wp_nav_menu(array(
		"theme_location" => "right"
    )); ?>
      </nav>

      <!-- YELLOW SECTION UNDER HEADER -->
      <section class="yellow_header">
        <div class="icon_holder">
<!--            Facebook-->
            <a href="https://www.facebook.com/BasketBallCityNYC" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_facebook.png">
            </a>
<!--            Twitter-->
            <a href="https://twitter.com/basketballcity" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_twitter.png">
            </a>
<!--            YouTube-->
            <a href="https://www.youtube.com/user/basketballcity23" target="_blank">    
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_video.png">
            </a>
<!--            Snapchat-->
            <a href="https://www.snapchat.com/add/Basketball_City" target="_blank">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_snapchat.png">
            </a>
            
            <a href="mailto:info@basketballcity.com">
                <img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/img/W_letter.png">
            </a>
                
        </div>
        <h3 class="logo_text">BASKETBALL CITY</h3>
<!--
        <form class="search">
          <input type="text" name="search_bar" placeholder="Search">
        </form>
-->
      </section>
        
        
<!--
        <nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><//?php esc_html_e( 'Primary Menu', 'emagid' ); ?></button>
			<//?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav>
-->
    </header>
