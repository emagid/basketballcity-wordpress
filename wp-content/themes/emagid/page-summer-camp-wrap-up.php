<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emagid
 */

get_header(); ?>


     <!-- JUMBOTRON -->

     <section class="jumbotron" style="background-image: url(<?php the_field('jumbotron_image'); ?>);">
       <div class="jumbotron_content">
         <h1 style="color:red !important;"><?php the_field('jumbotron_title'); ?></h1>
       </div>
     </section>




      <!-- CONTACT SECTION -->
     <section class="contact">
       <h3><?php the_field('rules'); ?></h3>
     </section>

     <!-- SCHEDULE SECTION -->
     <section class='list_section' style="text-align:center; display:block;">
      <div class="list_content" >
          <h2><?php the_field('gallery_title'); ?></h2>
          <?php the_field('content'); ?>
         </div>
     </section>


      <!-- PICTURE SECTION -->
     <section class="pictures">
         
    <?php
            $args = array(
      'post_type' => 'gallery',
        'posts_per_page' => 99,
                'cat' => '18'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>    
         
       <div class="pic" style="background-image: url(<?php the_field('image'); ?>);">
         <div class="overlay">
           <p><?php the_field('title'); ?></p>
         </div>
       </div>
         
      <?php
            }
                  }
            else  {
            echo 'No Images Found';
            }
      ?>
       <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>       
         

        
     </section>

         <div class="last-section" style="">
             <?php the_field('bottom_text'); ?>
            <img style="width:100%;" src="<?php the_field('bottom_image'); ?>">
         </div>

<script>
$("#register").click(function(){
        $('html, body').animate({
        scrollTop: $(".league_content").offset().top
    }, 2000);
    $(".league_content > div").hide();
        $(".register_content").toggle();
});
    
    $("#rules").click(function(){
        $('.info_holder button').removeClass('show_less');
        $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".rules_content").toggle();
});
    
        $("#divisions, #standings").click(function(){
            $('.info_holder button').removeClass('show_less');
            $(this).toggleClass('show_less');
    $(".league_content > div").hide();
        $(".schedule_content").toggle();
});
</script>
<?php
get_footer();
